Pod::Spec.new do |s|
          #1.
          s.name               = "KmsofPodDemo"
          #2.
          s.version            = "1.0.0"
          #3.  
          s.summary         = "Demo Kmsof Pod"
          #4.
          s.homepage        = "http://www.kmsof.com"
          #5.
          s.license              = "MIT"
          #6.
          s.author               = "Ravina Gadhiya"
          #7.
          s.platform            = :ios, "9.0"
          #8.
          s.source              = { http: "http://kmsof.com/Umoove.zip", flatten: true }
          #9.
          s.vendored_frameworks = 'Umoove.framework'
          s.frameworks   = 'AVFoundation', 'CoreGraphics', 'CoreMotion', 'SystemConfiguration', 'CoreTelephony', 'UIKit', 'CoreMedia', 'CoreVideo', 'CoreData'
          s.xcconfig     =  { 'FRAMEWORK_SEARCH_PATHS' => '"$(PODS_ROOT)/KmsofPodDemo"' }
    end